#!/usr/bin/python3
#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil
from gi.repository.GdkPixbuf import Pixbuf 
from gi.repository import Gio 

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_categories_page(widget, Gtk, content, window):
    
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)
    
    
    #######################vars###########################
    categories = []
    categories_data = get_categories()
    for category in categories_data:
        if 'childrens' in category.keys():
            for sub_category in category['childrens']:
                name = category['name'] + ":" + sub_category['name']
                categories.append([name, sub_category['tid']])
        else:
            name = category['name']
            categories.append([name, category['tid']])
    
    header.set_markup("<big><big>Categories</big></big>")
    print(categories)
    ###########################applist##############################

    categories_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
    
    #apps_scroll_box = Gtk.VBox()
    cat_boxes = []
    cat_buttons = []
    for cat in categories:
        #print(app['package']['name'])
        cat_boxes.append(Gtk.Box())
        button_txt = cat[0]
        cat_buttons.append(Gtk.Button(button_txt))
        cat_buttons[-1].connect("clicked", show_app_list_page_handle, Gtk, content, window, cat[1])
        
        cat_boxes[-1].pack_start(cat_buttons[-1], 1, 1, 0)
        categories_box.pack_start(cat_boxes[-1], 1, 0, 0)

    
    app_scroll_window = Gtk.ScrolledWindow()
    app_scroll_window.add_with_viewport(categories_box)


    ###########################Back button##############################
    back_button =  Gtk.Button()
    back_image = Gtk.Image.new_from_file( script_path + '/img/back.png')
    back_button.add(back_image)
    back_button.connect("clicked", display_main_page, Gtk, content, window)
    #categories_box.pack_start(back_button, 1, 1, 0)
    
    #setup selection window
    #left, top, width, height
    control_box = Gtk.Box()
    control_box.pack_start(back_button, 0, 0, 0)
    categories_box.pack_start(control_box, 0, 0, 0)


    #setup main window
    main_area = Gtk.Stack()
    main_area.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
    main_area.set_transition_duration(500)
    
    content.pack_start(app_scroll_window, True, True, 0)
    
    update_window(window)
    #print(dir(content))

#this is here to pass in the keyward arguments
def load_new_page(widget, Gtk, content, window, query, page):
    show_app_list_page(widget, Gtk, content, window, query=query, page=page)

def show_app_list_page_handle(widget, Gtk, content, window, category):
    show_app_list_page(widget, Gtk, content, window, category_id=category)
