#!/usr/bin/python3
#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil
from gi.repository.GdkPixbuf import Pixbuf 
from gi.repository import Gio 

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_app_view_page(widget, Gtk, content, window, appid):
    global flatpaks
    if appid in flatpaks:
        a_flatpak = True
    else:
        a_flatpak = False
    
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    last_page = []
    last_page_header = header.get_text()
    for page_thing in page_stuff:
        if page_thing != header:
            last_page.append(page_thing)
            content.remove(page_thing)
    
    #######################build app_data###########################
    if not a_flatpak:
        app_data = get_app(appid)
        markup_data = html_to_markup(app_data['body'])
        name = app_data['package']['name']
        title = "<big><big>" + app_data['title'] + "</big></big>"
        user_name = app_data['user']['name']
    if a_flatpak:
        markup_data = flatpaks[appid][-1]
        name = appid
        title = "<big><big>" + name + "</big></big>"
        user_name = False

    ###########################applist##############################
    header.set_markup(title)
    
    app_description = Gtk.Label()
    app_description.override_color(0,text_color)
    app_description.set_line_wrap(True)
    app_description.set_markup(markup_data)
    
    #############################buttons#############################
    back_button =  Gtk.Button()
    back_image = Gtk.Image.new_from_file( script_path + '/img/back.png')
    back_button.add(back_image)
    back_button.connect("clicked", show_last_page, Gtk, content, window, last_page, last_page_header)
    #apps_box.pack_start(back_button, 1, 1, 0)
    
    install_button =  Gtk.Button()
    install_button_image = Gtk.Image.new_from_file( script_path + '/img/install.png')
    install_button.add(install_button_image)
    install_button.connect("clicked", run_install, Gtk, content, window, name, user_name)

    app_ctl_box = Gtk.Box()
    app_ctl_box.pack_start(back_button, 0, 0, 0)
    #app_ctl_box.pack_start(next_page, 0, 0, 0) #TODO image button
    app_ctl_box.pack_start(install_button, 0, 0, 0)

    
    ############################pack it up##############################
    page_scroll = Gtk.ScrolledWindow()
    page_scroll.add_with_viewport(app_description)
    page_scroll.set_min_content_height(450)
    
    page_stuff = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
    page_stuff.pack_start(page_scroll, 0, 0, 0)
    page_stuff.pack_start(app_ctl_box, 0, 0, 0)
    
    content.pack_start(page_stuff, 0, 0, 0)
    update_window(window)
    #print(dir(content))

#this is here to pass in the keyward arguments
def load_new_page(widget, Gtk, content, window, query, page):
    show_app_list_page(widget, Gtk, content, window, query=query, page=page)

def run_install(widget, Gtk, content, window, package_name, repo_owner):
    global flatpaks
    #check if this is a flatpak
    if not repo_owner:
        pkg_name = flatpaks[package_name][1].split("/")[-1]
        #print (pkg_name)
        #print (flatpaks[package_name])
        cmd = f"kgx --command \"sudo flatpak install {pkg_name}\""
    else:
        print("install: " + package_name + " " + repo_owner)
        #run:
        #         ./bin/install <pkg_name> <repo_owner>
        #as root
        #    sudo
        #in kings cross
        #kgx
        cmd = "kgx --command \"" + "sudo " + script_path + "bin/install '" + package_name + "' '" + repo_owner + "'\""
    print(cmd)
    os.system(cmd)
