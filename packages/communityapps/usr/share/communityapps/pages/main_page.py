#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#import os
from gi.repository.GdkPixbuf import Pixbuf, InterpType 

script_path = os.path.dirname(os.path.realpath(__file__))[:-5]
print(script_path)
print(script_path)
print(script_path)
def display_main_page(widget, Gtk, content, window):


    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)
            
    header.set_markup("<big><big>Welcome</big></big>")
    ###########################Main grid##############################

    #Setup grid on main page
    #Top Left
    updated_button =  Gtk.Button()
    updated_button_image = Gtk.Image.new_from_file( script_path + '/img/updated.png')
    updated_button.add(updated_button_image)
    updated_button.connect("clicked", show_app_list_page, Gtk, content, window)
    #Top Right
    search_button =  Gtk.Button()
    search_button_image = Gtk.Image.new_from_file( script_path + '/img/search.png')
    search_button.add(search_button_image)
    search_button.connect("clicked", show_search_page, Gtk, content, window)
    #Bot Left
    categories_button =  Gtk.Button()
    categories_button_image = Gtk.Image.new_from_file( script_path + '/img/categories.png')
    categories_button.add(categories_button_image)
    categories_button.connect("clicked", show_categories_page, Gtk, content, window)
    #Bot Right
    remove_button =  Gtk.Button()
    remove_image = Gtk.Image.new_from_file( script_path + '/img/remove.png')
    remove_button.add(remove_image)
    remove_button.connect("clicked", show_remove_page, Gtk, content, window)
    #Very Bottom
    flatpak_button = Gtk.Button("Flatpak repository")
    flatpak_button.connect("clicked", show_flatpak_page, Gtk, content, window)
    
    main_grid = Gtk.Grid(column_homogeneous=True,
                            column_spacing=2,
                            row_spacing=2)
    #left, top, width, height
    main_grid.attach(updated_button, 0, 0, 1, 1)
    main_grid.attach(search_button, 1, 0, 1, 1)
    main_grid.attach(categories_button, 0, 1, 1, 1)
    main_grid.attach(remove_button, 1, 1, 1, 1)
    main_grid.attach(flatpak_button, 0, 2, 2, 1)
    
    content.pack_start(main_grid, True, True, 0)
    #content.pack_start(back_button, expand=True, fill=False, padding = 0)
    update_window(window)


