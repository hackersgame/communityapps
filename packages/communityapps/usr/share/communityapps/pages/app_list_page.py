#!/usr/bin/python3
#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil
from gi.repository.GdkPixbuf import Pixbuf 
from gi.repository import Gio 

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_app_list_page(widget, Gtk, content, window, query=None, page=0, category_id=None):
    global flatpaks
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    last_page = []
    flatpaks_to_show = []
    last_page_header = header.get_text()
    for page_thing in page_stuff:
        if page_thing != header:
            last_page.append(page_thing)
            content.remove(page_thing)
    
    
    #######################build app_data###########################
    if query != None:
        title_text = "Search: " + query + " | Page: " + str(page + 1)
        #openrepo data
        app_data = search_openrepos(query, page=page)
        #flatpak data:
        if flatpaks == {}:
            load_flatpaks()
        for flatpak_name in flatpaks:
            flatpak_info = flatpaks[flatpak_name][-1] + flatpak_name
            flatpak_info = flatpak_info.lower()
            if query in flatpak_info:
                flatpaks_to_show.append(flatpak_name)
        print(app_data)
        print(flatpaks_to_show)
    elif category_id != None:
        title_text = "Browse"
        app_data = get_apps_by_category(category_id, page=page)
    else:
        title_text = "Recent: | Page: " + str(page + 1)
        #openrepo data
        app_data = get_recent(page=page)
        #flatpak data:
        if flatpaks == {}:
            load_flatpaks()
        flatpaks_to_show = list(flatpaks.keys())
        print(app_data)
    
    header.set_markup("<big><big>" + title_text + "</big></big>")
    ###########################applist##############################
    select_file = Gtk.Button("Open")
    #select_file.connect("clicked", select_a_book, Gtk)
    select_file.set_halign(Gtk.Align.END)

    next_page = Gtk.Button("Next")
    next_page.connect("clicked", load_new_page, Gtk, content, window, query, page+1, category_id)
    next_page.set_halign(Gtk.Align.END)

    previous_page = Gtk.Button("previous")
    if page > 0:
        previous_page.connect("clicked", load_new_page, Gtk, content, window, query, page-1, category_id)
    previous_page.set_halign(Gtk.Align.END)

    apps_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
    
    #apps_scroll_box = Gtk.VBox()
    app_boxes = []
    app_buttons = []
    app_bg_img = []
    
    #add openrepo apps
    for app in app_data:
        #print(app['package']['name'])
        app_boxes.append(Gtk.Box())
        button_txt = app['title'] + " | "
        button_txt = button_txt + str(int(float(app['rating']['rating']))) + "%"
        app_buttons.append(Gtk.Button(button_txt))
        app_buttons[-1].connect("clicked", show_app_view_page, Gtk, content, window, app['appid'])
        print(app['appid'])
        
        app_boxes[-1].pack_start(app_buttons[-1], 1, 1, 0)
        apps_box.pack_start(app_boxes[-1], 1, 0, 0)

    #add flatpaks
    for app in flatpaks_to_show:
        app_boxes.append(Gtk.Box())
        button_txt = app + " | Flathub"
        app_buttons.append(Gtk.Button(button_txt))
        app_buttons[-1].connect("clicked", show_app_view_page, Gtk, content, window, app)
        
        app_boxes[-1].pack_start(app_buttons[-1], 1, 1, 0)
        apps_box.pack_start(app_boxes[-1], 1, 0, 0) 


    app_scroll_window = Gtk.ScrolledWindow()
    app_scroll_window.add_with_viewport(apps_box)


    ###########################Back button##############################
    back_button =  Gtk.Button()
    back_image = Gtk.Image.new_from_file( script_path + '/img/back.png')
    back_button.add(back_image)
    back_button.connect("clicked", show_last_page, Gtk, content, window, last_page, last_page_header)
    #apps_box.pack_start(back_button, 1, 1, 0)
    
    #setup selection window
    #left, top, width, height
    control_box = Gtk.Box()
    control_box.pack_start(back_button, 0, 0, 0)
    control_box.pack_start(previous_page, 0, 0, 0)
    control_box.pack_start(next_page, 0, 0, 0)
    apps_box.pack_start(control_box, 0, 0, 0)


    #setup main window
    main_area = Gtk.Stack()
    main_area.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
    main_area.set_transition_duration(500)
    
    content.pack_start(app_scroll_window, True, True, 0)
    update_window(window)
    #print(dir(content))

#this is here to pass in the keyward arguments
def load_new_page(widget, Gtk, content, window, query, page, category_id):
    show_app_list_page(widget, Gtk, content, window, query=query, page=page, category_id=category_id)
