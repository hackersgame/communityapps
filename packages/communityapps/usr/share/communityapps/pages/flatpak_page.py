#!/usr/bin/python3
#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil
import subprocess
from gi.repository.GdkPixbuf import Pixbuf 
from gi.repository import Gio 

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_flatpak_page(widget, Gtk, content, window):
    
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)
    
    #####################Check flatpak status#######################
    if we_have_flathub(''):
        action_button = Gtk.Button("Remove Flathub")
        action_button.connect("clicked", remove_flathub, Gtk, content, window)
    else:
        action_button = Gtk.Button("Install Flathub repo")
        action_button.connect("clicked", add_flathub, Gtk, content, window)

    header.set_markup("Flathub may contain nonfree software!")
    ###########################pack stuff##############################
    flatpack_action_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
    flatpak_scroll_window = Gtk.ScrolledWindow()
    flatpak_scroll_window.add_with_viewport(flatpack_action_box)
    flatpack_action_box.pack_start(action_button, 1, 0, 0)

    ###########################Back button##############################
    back_button =  Gtk.Button()
    back_image = Gtk.Image.new_from_file( script_path + '/img/back.png')
    back_button.add(back_image)
    back_button.connect("clicked", display_main_page, Gtk, content, window)
    
    #setup selection window
    #left, top, width, height
    control_box = Gtk.Box()
    control_box.pack_start(back_button, 0, 0, 0)
    flatpack_action_box.pack_start(control_box, 0, 0, 0)

    
    content.pack_start(flatpak_scroll_window, True, True, 0)
    update_window(window)
    #print(dir(content))

def add_flathub(widget, Gtk, content, window):
    cmd = "kgx --command \"" + "sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo" +"\""
    os.system(cmd)
    display_main_page(widget, Gtk, content, window)

def we_have_flathub(na):
    result = subprocess.run(['flatpak', 'remotes'], stdout=subprocess.PIPE)
    cmd_out = result.stdout.decode('utf-8')
    cmd_out = cmd_out.strip()
    if cmd_out == "":
        return (False)
    else:
        for flatpakrepo in cmd_out.split('\n'):
            if "flathub" in flatpakrepo:
                return (True)
    return(False)

def remove_flathub(widget, Gtk, content, window):
    #run:
    #         flatpak remote-delete flathub
    #as root
    #    sudo
    #in kings cross
    #kgx
    cmd = "kgx --command \"" + "sudo flatpak remote-delete flathub" +"\""
    print(cmd)
    os.system(cmd)
    display_main_page(widget, Gtk, content, window)
