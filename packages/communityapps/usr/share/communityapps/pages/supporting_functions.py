#!/usr/bin/python3

#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import requests
import json
import urllib
import re
from gi.repository import Gdk

###############################Vars##############################
api_base = 'https://openrepos.net/api/v1/'
app_search = "search/apps"

platform_header = {'Warehouse-Platform': 'PureOS'}
#platform_header = {'Warehouse-Platform': 'SailfishOS'}
text_color = Gdk.RGBA(red=1.0, green=1.0, blue=1.0, alpha=1.0)
flatpaks = {}

def load_flatpaks():
    global flatpaks
    base_url = 'https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development'
    fp = urllib.request.urlopen(base_url)
    raw_site = fp.read()
    raw_site = raw_site.decode("utf8")
    fp.close()
    
    for line in raw_site.split('\n'):
        #look for app tables
        if 'App Source' in line:
            for table in line.split("App Source")[1:]:
                if "<strong>Pkg</strong>" not in table:
                    continue
                table = table.split("</table>")[0]
                for table_row in table.split("<tr"):
                    #a row looks like:
                    #data-sourcepos="67:1-67:186">&#x000A;<td data-sourcepos="67:2-67:50"><a href="https://gitlab.com/cunidev/workflow" rel="nofollow noreferrer noopener" target="_blank">Workflow</a></td>&#x000A;<td data-sourcepos="67:52-67:124"><a href="https://flathub.org/apps/details/com.gitlab.cunidev.Workflow" rel="nofollow noreferrer noopener" target="_blank">Flathub</a></td>&#x000A;<td data-sourcepos="67:126-67:138">GTK, Python</td>&#x000A;<td data-sourcepos="67:140-67:185">A screen time monitor based on ActivityWatch</td>&#x000A;</tr>&#x000A;</tbody>&#x000A;
                    broken_on_links = table_row.split('<a href="')
                    if len(broken_on_links) > 2:
                        project_link = broken_on_links[1].split('"')[0]
                        project_name = broken_on_links[1].split('>')[1].split("<")[0]
                        install_link = broken_on_links[2].split('"')[0]
                        description = table_row.split("<td")[-1].split(">")[1].split('<')[0]
                        if 'flathub' in install_link:
                            print(f"Flatpak package: '{project_name}' Install: {install_link}")
                            flatpaks[project_name] = [project_link, install_link, description]

def installed_via_flathub():
    result = subprocess.run(['flatpak', 'list', '--app', '--columns=application'], stdout=subprocess.PIPE)
    cmd_out = result.stdout.decode('utf-8')
    cmd_out = cmd_out.strip()
    list_of_flatpaks = {}
    for flatpak_app in cmd_out.split('\n'):
        name = flatpak_app.split(".")[-1]
        list_of_flatpaks[name] = flatpak_app
    return(list_of_flatpaks)

    
def search_openrepos(query, page=0):
    query = urllib.parse.quote_plus(query)
    full_api_call = api_base + app_search + "?keys=" + query + "&page=" + str(page)
    r = requests.get(full_api_call, headers=platform_header)
    apps = json.loads(r.text)
    return(apps)

def get_recent(page=0):
    full_api_call = api_base + "apps?page=" + str(page)
    r = requests.get(full_api_call, headers=platform_header)
    apps = json.loads(r.text)
    return(apps)

def get_apps_by_category(category_id, page=0):
    full_api_call = api_base + "/categories/" + category_id + "/apps?page=" + str(page)
    r = requests.get(full_api_call, headers=platform_header)
    apps = json.loads(r.text)
    return(apps)
    
def get_app(appid):
    full_api_call = api_base + "apps/" + str(appid)
    r = requests.get(full_api_call, headers=platform_header)
    app_info = json.loads(r.text)
    return(app_info)

def get_categories():
    full_api_call = api_base + "/categories"
    r = requests.get(full_api_call, headers=platform_header)
    app_info = json.loads(r.text)
    print(app_info)
    return(app_info)

def html_to_markup(html):
    html = html.replace("<p>", "\n")
    html = html.replace("</p>", "\n")
    html = html.replace("<h1>", "<[big]><[big]>")
    html = html.replace("</h1>", "<[/big]><[/big]>")
    html = html.replace("<h2>", "<[big]>")
    html = html.replace("</h2>", "<[/big]>")
    
    #remove other tags
    other_tags = re.findall(r'<(?!\[).+?>', html)
    for bad_tag in other_tags:
        #simplify links
        if bad_tag.startswith('<a'):
            url = bad_tag.replace("'", '"').split('"')[1]
            new_a_tag = "<a href='" + url + "'>"
            html = html.replace(bad_tag, new_a_tag)
        elif bad_tag.startswith('</a'):
            continue
        elif '/' in bad_tag:
            html = html.replace(bad_tag, '\n')
        else:
            html = html.replace(bad_tag, '')

    #fix tags
    html = html.replace("<[", "<")
    html = html.replace("]>", ">")
    return(html)

def show_last_page(widget, Gtk, content, window, last_page, last_page_header):
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)
    header.set_markup("<big><big>" + last_page_header + "</big></big>")
    
    for last_page_item in last_page:
        content.pack_start(last_page_item, True, True, 0)
    update_window(window)
