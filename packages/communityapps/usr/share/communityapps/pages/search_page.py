#!/usr/bin/python3
#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_search_page(widget, Gtk, content, window):
    global speed_label
    
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)

    header.set_markup("<big><big>Search for apps:</big></big>")

    ###########################Search page##############################
    search_box = Gtk.Entry()
    search_box.set_size_request(250,0)
    

    query_button = Gtk.Button(label="Search")
    query_button.connect("clicked", search_handle, Gtk, content, window, search_box)
    
    
    search_grid = Gtk.Grid(row_spacing=50, column_spacing=12)
    search_grid.attach(search_box,  0, 0, 1, 1)
    search_grid.attach(query_button,  2, 0, 1, 1)


    ###########################Back button##############################
    back_button =  Gtk.Button()
    back_image = Gtk.Image.new_from_file( script_path + '/img/back.png')
    back_button.add(back_image)
    back_button.connect("clicked", display_main_page, Gtk, content, window)
    

    #setup main window
    main_area = Gtk.Stack()
    main_area.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
    main_area.set_transition_duration(500)
    
    content.pack_start(search_grid, True, True, 0)
    content.pack_start(back_button, False, False, 0)
    update_window(window)

def search_handle(widget, Gtk, content, window, search_box):
    search_txt = search_box.get_text()
    if search_txt != "":
        show_app_list_page(widget, Gtk, content, window, query=search_txt)
