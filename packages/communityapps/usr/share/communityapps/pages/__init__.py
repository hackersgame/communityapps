
import os
import sys

########################################################
##################### Import pages######################
########################################################
page_path = os.path.dirname(os.path.realpath(__file__))
init_pages = []


#init_pages
for file_with_cm_ds in os.listdir(page_path):

    #filter stuff
    if "__" in file_with_cm_ds:
        continue
    #builds somthing like:  ['rename', 'tell'] by reading ./*
    init_pages.append(file_with_cm_ds.split('.py')[0])

sys.path.insert(0, page_path + '/')


for page in init_pages:
    script = ""
    page = page.strip(".")
    print("Loading: " + page)
    page_file = page_path + '/' + page + ".py"
    #print(page_file)
    print("     File: " + page_file)
    for python_line in open(page_file).readlines():
        script = script + python_line
    try:
        exec(script)
    except Exception as e:
        print("Running: " + script)
        print('Error Loading ' + page_file)
        #Error loading, Exit with error
        raise e
        #exit()


def update_window(window):
    window.set_border_width(15)
    window.set_default_size(360,720)
    window.show_all()
