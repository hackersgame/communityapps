#!/usr/bin/python3
#communityApps GPL3
#Copyright (C) 2020 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil
from gi.repository.GdkPixbuf import Pixbuf 
from gi.repository import Gio 

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_remove_page(widget, Gtk, content, window):
    
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)
    
    
    #######################read openrepos###########################
    installed_via_openrepos = {}
    repo_dir = "/etc/apt/sources.list.d"
    for repo_file in os.listdir(repo_dir):
        repo_file = os.path.join(repo_dir, repo_file)
        if "openrepos" in repo_file:
            with open(repo_file) as fh:
                file_data = fh.readlines()
            #first line in the repo file should be:
            #<package> <repo>
            #example: #phonic basil
            if file_data[0].startswith('#'):
                package_to_remove = file_data[0].split(" ")[0].strip("#").strip()
                repo_owner = file_data[0].split(" ")[-1].strip()
                installed_via_openrepos[package_to_remove] = repo_owner
    
    header.set_markup("<big><big>Remove</big></big>")
    ###########################applist##############################
    apps_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
    
    #apps_scroll_box = Gtk.VBox()
    app_boxes = []
    app_buttons = []
    app_bg_img = []
    for app in installed_via_openrepos.keys():
        #print(app['package']['name'])
        app_boxes.append(Gtk.Box())
        button_txt = app + " | By " + installed_via_openrepos[app]
        app_buttons.append(Gtk.Button(button_txt))
        app_buttons[-1].connect("clicked", run_remove, Gtk, content, window, app, installed_via_openrepos[app])
        
        app_boxes[-1].pack_start(app_buttons[-1], 1, 1, 0)
        apps_box.pack_start(app_boxes[-1], 1, 0, 0)
    
    installed_flatpaks = installed_via_flathub()
    for app_name in installed_flatpaks:
        app_package = installed_flatpaks[app_name]
        
        app_boxes.append(Gtk.Box())
        button_txt = app_name + " | Flatpak"
        app_buttons.append(Gtk.Button(button_txt))
        app_buttons[-1].connect("clicked", run_remove, Gtk, content, window, app_package, False)
        
        app_boxes[-1].pack_start(app_buttons[-1], 1, 1, 0)
        apps_box.pack_start(app_boxes[-1], 1, 0, 0)
    
    app_scroll_window = Gtk.ScrolledWindow()
    app_scroll_window.add_with_viewport(apps_box)



    ###########################Back button##############################
    back_button =  Gtk.Button()
    back_image = Gtk.Image.new_from_file( script_path + '/img/back.png')
    back_button.add(back_image)
    back_button.connect("clicked", display_main_page, Gtk, content, window)
    #apps_box.pack_start(back_button, 1, 1, 0)
    
    #setup selection window
    #left, top, width, height
    control_box = Gtk.Box()
    control_box.pack_start(back_button, 0, 0, 0)
    apps_box.pack_start(control_box, 0, 0, 0)

    #setup main window
    main_area = Gtk.Stack()
    main_area.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
    main_area.set_transition_duration(500)
    
    content.pack_start(app_scroll_window, True, True, 0)
    update_window(window)
    #print(dir(content))


def run_remove(widget, Gtk, content, window, package_name, repo_owner):
    if not repo_owner:
        cmd = f"kgx --command \"sudo flatpak remove {package_name}\""
    else:
        print("remove: " + package_name + " " + repo_owner)
        #run:
        #         ./bin/remove <pkg_name> <repo_owner>
        #as root
        #    sudo
        #in kings cross
        #kgx
        cmd = "kgx --command \"" + "sudo " + script_path + "bin/remove '" + package_name + "' '" + repo_owner + "'\""
    print(cmd)
    os.system(cmd)
    display_main_page(widget, Gtk, content, window)
